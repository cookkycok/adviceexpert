function makeid(length) {
    let result           = '';
    const characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    const charactersLength = characters.length;
    for ( let i = 0; i < length; i++ ) {
        result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
}

function fakeMail(domain = makeid(10) + '.com') {
    return (makeid(10) + '@' + domain).toLowerCase();
}

function fakePassword() {
    return '1a' + makeid(10);
}

module.exports = {
    fakeMail,
    fakePassword,
    makeid,
}

