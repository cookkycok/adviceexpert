const chai = require('chai');
const chaiHttp = require('chai-http');
const faker = require('faker');
const fakerDE = require('faker/locale/de');
const fakerRU = require('faker/locale/ru');
const fakerUA = require('faker/locale/uk');

const {fakeMail, makeid, fakePassword} = require('./utils');

chai.use(chaiHttp);

const {expect} = chai;
const server = chai.request('http://localhost:4000');
const apiPath = '/api/user';

describe("User", function() {
    describe("Registration", function() {
        it("without email", async function() {
            const answer = await server
                .put(`${apiPath}/register`);
            expect(answer.status).to.equal(401);
            expect(answer.body.message).to.equal('Invalid email');
        });

        it("with bad email", async function() {
            const answer = await server
                .put(`${apiPath}/register`)
                .send({email: 'test fff@mail.com'});

            expect(answer.status).to.equal(401);
            expect(answer.body.message).to.equal('Invalid email');
        });

        it("without password", async function() {
            const answer = await server
                .put(`${apiPath}/register`)
                .send({email: 'testfff@mail.com'});
            expect(answer.status).to.equal(401);
            expect(answer.body.message).to.equal('Invalid password');
        });

        it("without name", async function() {
            const answer = await server
                .put(`${apiPath}/register`)
                .send({
                    email: 'testfff@mail.com',
                    password: '21212DSfssf'
                });
            expect(answer.status).to.equal(401);
            expect(answer.body.message).to.equal('Invalid name');
        });

        describe("International", function() {

            it("EN user", async function() {
                const email = fakeMail();
                const name = faker.name.findName();
                const password = fakePassword();
                const answer = await server
                    .put(`${apiPath}/register`)
                    .send({
                        email,
                        name,
                        password,
                    });
                expect(answer.status).to.equal(200);

                const retryAnswer = await server
                    .put(`${apiPath}/register`)
                    .send({
                        email,
                        name,
                        password,
                    });
                expect(retryAnswer.status).to.equal(401);
                expect(retryAnswer.body.message).to.equal('User already exist');
            });

            it("DE user", async function() {
                const email = fakeMail();
                const name = fakerDE.name.findName();
                const password = fakePassword();
                const answer = await server
                    .put(`${apiPath}/register`)
                    .send({
                        email,
                        name,
                        password,
                    });
                expect(answer.status).to.equal(200);
            });

            it("RU user", async function() {
                const email = fakeMail();
                const name = fakerRU.name.findName();
                const password = fakePassword();
                const answer = await server
                    .put(`${apiPath}/register`)
                    .send({
                        email,
                        name,
                        password,
                    });
                expect(answer.status).to.equal(200);
            });

            it("UA user", async function() {
                const email = fakeMail();
                const name = fakerUA.name.findName();
                const password = fakePassword();
                const answer = await server
                    .put(`${apiPath}/register`)
                    .send({
                        email,
                        name,
                        password,
                    });
                expect(answer.status).to.equal(200);
            });

            it("EN gmail user", async function() {
                const first = makeid(7);
                const second = makeid(7);
                const email = `${first}${second}@gmail.com`;
                const name = faker.name.findName();
                const password = fakePassword();
                const answer = await server
                    .put(`${apiPath}/register`)
                    .send({
                        email,
                        name,
                        password,
                    });
                expect(answer.status).to.equal(200);

                const retryAnswer = await server
                    .put(`${apiPath}/register`)
                    .send({
                        email: `${first}.${second}@gmail.com`,
                        name,
                        password,
                    });
                expect(retryAnswer.status).to.equal(401);
                expect(retryAnswer.body.message).to.equal('User already exist');
            });
        });
    });

    describe("Authorization", function() {
        it("without email", async function() {
            const answer = await server
                .post(`${apiPath}/auth`);
            expect(answer.status).to.equal(401);
            expect(answer.body.message).to.equal('Invalid email');
        });

        it("with bad email", async function() {
            const answer = await server
                .post(`${apiPath}/auth`)
                .send({email: 'test fff@mail.com'});

            expect(answer.status).to.equal(401);
            expect(answer.body.message).to.equal('Invalid email');
        });

        it("without password", async function() {
            const answer = await server
                .post(`${apiPath}/auth`)
                .send({email: 'testfff@mail.com'});
            expect(answer.status).to.equal(401);
            expect(answer.body.message).to.equal('Email or password incorrect');
        });

        it("new user", async function() {
            const first = makeid(8);
            const second = makeid(8);
            const email = `${first}${second}@gmail.com`;
            const password = fakePassword();
            const answer = await server
                .post(`${apiPath}/auth`)
                .send({email, password});
            expect(answer.status).to.equal(401);
            expect(answer.body.message).to.equal('User not found');
        });

        it("auth user", async function() {
            const email = fakeMail();
            const name = faker.name.findName();
            const password = fakePassword();
            const answerReg = await server
                .put(`${apiPath}/register`)
                .send({
                    email,
                    name,
                    password,
                });
            expect(answerReg.status).to.equal(200);

            const answer = await server
                .post(`${apiPath}/auth`)
                .send({email, password});
            expect(answer.status).to.equal(200);
        });

        it("block brut password", async function() {
            this.timeout(10000);
            const first = makeid(8);
            const second = makeid(8);
            const name = faker.name.findName();
            const email = `${first}${second}@gmail.com`;
            const passwordCorrect = fakePassword();
            const password = fakePassword();
            const answerReg = await server
                .put(`${apiPath}/register`)
                .send({
                    email,
                    name,
                    password: passwordCorrect,
                });

            const {headers: {'x-request-id': xRequest}} = answerReg;

            await server
                .post(`${apiPath}/auth`)
                .set('x-request-id', xRequest)
                .send({email, password});
            await server
                .post(`${apiPath}/auth`)
                .set('x-request-id', xRequest)
                .send({email, password});
            await server
                .post(`${apiPath}/auth`)
                .set('x-request-id', xRequest)
                .send({email, password});
            await server
                .post(`${apiPath}/auth`)
                .set('x-request-id', xRequest)
                .send({email, password});
            const answer = await server
                .post(`${apiPath}/auth`)
                .set('x-request-id', xRequest)
                .send({email, password});

            expect(answer.status).to.equal(401);
            expect(answer.body.message).to.equal('Request blocked. Try later.');
        });

    });
});
