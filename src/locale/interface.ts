export enum LocaleKeys {
    titleProject = 'titleProject',
}

export enum LocaleLangs {
    en = 'en',
    ru = 'ru',
    uk = 'uk',
}

export type Locale = {
    [key in keyof LocaleKeys]: string;
}

export type LocaleGlobal = {
    [lang in keyof LocaleLangs]: Locale;
}
