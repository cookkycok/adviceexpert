import {Locale, LocaleGlobal, LocaleLangs} from '@locale/interface';

const langObject: LocaleGlobal = process.env.LOCALE as any;

export function getLocale(lang: LocaleLangs): Locale | Promise<Locale> {
    if(process.env.SERVER) {
        return langObject[lang];
    }
    return import(`./${lang}.json`);
}
