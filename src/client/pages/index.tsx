import React, {ReactElement} from 'react';

import {PageNames} from '@route/routes';
import Home from '@page/Home';

export function Pages({route: {name}}: any): ReactElement {
    switch (name) {
    case PageNames.home:
        return <Home />;
    case PageNames.app:
        return <Home />;
    }

    return (
        <div>
            Not Found
        </div>
    );
}
