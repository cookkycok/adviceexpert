import React, {ReactElement} from 'react';
import {Link, routeNode} from 'react-router5';

import {PageNames} from '@route/routes';
import Logo from '@icon/logo.svg';
import style from './style.scss';

function Home({router}: any): ReactElement {
    console.log('style: ', style);
    return (
        <div className={style.test}>
            Home Hello 111111111111
            {' '}
            <Logo />
            <Link
                routeName={'app'}
                router={router}
            >
                GO to app 2
            </Link>
        </div>
    );
}

export default routeNode(PageNames.home)(Home);
