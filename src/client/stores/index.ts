import locale from './locale';
import router from './router';
import search from './search';

export const storeCombine = {
    locale,
    router,
    search,
};

if (!process.env.SERVER) {
    (window as any).__STORES__ = storeCombine;
}
