import {observable, action} from 'mobx';

import {getLocale} from '@locale';
import {LocaleLangs} from '@locale/interface';

class LocaleStore {
	@observable lang = LocaleLangs.en;
	@observable loaded = false;
	@observable.ref data = {};

	constructor() {
	    this.loadLocale(this.lang);
	}

	getLocale = (key: LocaleLangs): string => this.data[key] || '';

	@action loadLocale = async (lang: LocaleLangs): Promise<void> => {
	    this.loaded = false;
	    this.data = await getLocale(this.lang);
	    this.lang = lang;
	    this.loaded = true;
	}
}

export default new LocaleStore();



