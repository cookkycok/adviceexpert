import {action, observable} from 'mobx';
import {httpRequest} from '@util/request';

class SearchStore {
	@observable.ref result = [];

	@action
	public find = async (text: string): Promise<void> => {
	    const result = await httpRequest.get('search', {
	    	searchParams: {
	    		query:  text,
	        },
	    }).json();
	    console.log('result: ', result);
	}
}

export default new SearchStore();



