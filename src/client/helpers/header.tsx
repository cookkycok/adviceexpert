import React, {FunctionComponent} from 'react';

import {Helmet} from "react-helmet";
import {observer, inject} from 'mobx-react';


const HeaderComponent: FunctionComponent = ({locale}: any) => (
    <Helmet>
        <html lang="en" />
        <meta charSet="utf-8" />
        <meta
            content="width=device-width, initial-scale=1.0"
            name="viewport"
        />
        <meta
            content="ie=edge"
            httpEquiv="X-UA-Compatible"
        />
        <title>
            {locale.getLocale('titleProject')}
        </title>
    </Helmet>
);

export const Header = inject('locale')(observer(HeaderComponent));
