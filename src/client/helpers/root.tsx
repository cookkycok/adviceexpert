import React, {FunctionComponent, ReactNode} from 'react';
import {RouterProvider, Route} from 'react-router5';
import {Provider} from 'mobx-react';
import {Router} from 'router5';
import {hot} from 'react-hot-loader/root';

import {Pages} from '@pages';
import {storeCombine} from '@store';
import {Header} from './header';

export const Application = hot<FunctionComponent<ApplicationProps>>(({router}: ApplicationProps) => (
    <RouterProvider router={router}>
        <Provider {...storeCombine}>
            <>
                <Header />
                <Route>
                    {({route}): ReactNode => <Pages route={route} />}
                </Route>
            </>
        </Provider>
    </RouterProvider>
));

interface ApplicationProps {
    router: Router;
}
