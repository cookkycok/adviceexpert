export enum PageNames {
    app = 'app',
    home = 'home',
    main = '',
}

export default [
    {name: PageNames.home, path: '/'},
    {name: PageNames.app, path: '/app'},
];
