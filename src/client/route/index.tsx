import createRouter, {Router} from 'router5';
import {mobxPlugin} from 'mobx-router5';

import routes from './routes';
import routerStore from '../stores/router';

const routerOptions = {
    defaultRoute: '/',
    strictQueryParams: true,
    trailingSlash: true,
};

export default function configureRouter(): Router {
    const router = createRouter(routes, routerOptions);

    if (!process.env.SERVER) {
        // eslint-disable-next-line @typescript-eslint/no-var-requires
        const browserPlugin = require('router5-plugin-browser');
        router.usePlugin((browserPlugin.default || browserPlugin)());
    }
    if (process.env.NODE_ENV !== 'production') {
        // eslint-disable-next-line @typescript-eslint/no-var-requires
        const logger = require('router5-plugin-logger');
        router.usePlugin(logger.default || logger);
    }

    router.usePlugin(mobxPlugin(routerStore));

    return router;
}
