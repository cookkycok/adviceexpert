import ky from 'ky';

export const httpRequest = ky.create({
    prefixUrl: 'http://localhost:3000/api',
    throwHttpErrors: false,
});
