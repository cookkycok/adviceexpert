import React from 'react';
import {hydrate} from 'react-dom';
import io from 'socket.io-client';

import {Application} from './helpers/root';
import createRouter from './route';

const mainRoute = createRouter();
const socket = io('http://localhost:4000', {transports: ['websocket', 'polling']});

socket.on('news', function (data) {
    console.log(data);
    socket.emit('my other event', {my: 'data'});
});

mainRoute.start(() => {
    hydrate(
        <Application
            router={mainRoute}
        />,
        document.getElementById('root')
    );
});

