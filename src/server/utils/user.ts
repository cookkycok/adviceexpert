import jwt from 'jsonwebtoken';

const JWT_SECURE_CODE = 'YWR2aWNlRXhwZXJ0';
const TIME_SESSION = "30d";

interface DecodeUserId {
	data: string | null;
	exp?: number;
	iat?: number;
}

export function encodeUserId(userId: string): string {
    return jwt.sign(
        {data: userId},
        JWT_SECURE_CODE,
        {
            expiresIn: TIME_SESSION,
        },
    );
}

export function decodeUserId(token: string): DecodeUserId {
    try {
        return jwt.verify(token, JWT_SECURE_CODE) as DecodeUserId;
    } catch (e) {
        return {data: null};
    }
}
