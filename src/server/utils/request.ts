import {ParameterizedContext} from 'koa';
import ky from 'ky-universal';

export function getBodyData<T = object>(ctx: ParameterizedContext): T {
    return ctx.request.body;
}

export function getQueryData<T = object>(ctx: ParameterizedContext): T {
    return ctx.request.query;
}

export const httpRequest = ky.create({
    prefixUrl: 'http://localhost:3000/api',
    throwHttpErrors: false,
});
