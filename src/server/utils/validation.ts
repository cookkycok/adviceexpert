import EmailValidator from 'email-validator';

const MIN_PASSWORD_LENGTH = 7;
const MAX_PASSWORD_LENGTH = 50;
const PASSWORD_REGEX =  {
    digits: /\d+/,
    letters: /[a-z]+/i,
    spaces: /[\s]+/,
};
const MIN_NAME_LENGTH = 2;
const MAX_NAME_LENGTH = 150;

function processCheck(password, regexp): boolean {
    return new RegExp(regexp).test(password);
}

export function isPasswordValid(password: string): boolean {
    const passwordLength = password.length;
    const isLengthCorrect = passwordLength >= MIN_PASSWORD_LENGTH && passwordLength <= MAX_PASSWORD_LENGTH;
    const hasDigits = processCheck(password, PASSWORD_REGEX.digits);
    const hasLetters = processCheck(password, PASSWORD_REGEX.letters);
    const hasSpace = processCheck(password, PASSWORD_REGEX.spaces);
    return isLengthCorrect && hasDigits && hasLetters && !hasSpace;
}

export function isEmailValid(email: string): boolean {
    return !!email.length && EmailValidator.validate(email);
}

export function isNameValid(name: string): boolean {
    const nameLength = name.length;
    const isLengthCorrect = nameLength >= MIN_NAME_LENGTH && nameLength <= MAX_NAME_LENGTH;
    // en-US uk-UA ru-RU da-DK de-DE pl-PL
    return isLengthCorrect && processCheck(name, /^[A-ZА-ыЩЬЮЯЄIЇҐіЁZÆØÅÄÖÜßĄĆĘŚŁŃÓŻŹ’]{2,}\.?(\s?[A-ZА-ыЩЬЮЯЄIЇҐіЁZÆØÅÄÖÜßĄĆĘŚŁŃÓŻŹ’.]+\.?)+$/i); // https://github.com/validatorjs/validator.js/blob/master/src/lib/alpha.js
}
