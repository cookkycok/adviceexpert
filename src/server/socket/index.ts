import socket from 'socket.io';
import socketRedis from 'socket.io-redis';
import {Server} from "http";

export function socketService(server: Server): void {
    const io = socket(server);

    io.adapter(socketRedis({host: 'localhost', port: 6379}));

    io.on('connection', function (socket) {
        socket.emit('news', {hello: 'world'});
        socket.on('my other event', function (data) {
            console.log(data);
        });
    });
}
