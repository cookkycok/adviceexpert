import {join} from 'path';
import {readFileSync} from 'fs';
import {Helmet} from "react-helmet";

import {
    applicationStaticName,
    runtimeStaticName,
    staticPath,
    stylesInlineName,
    stylesStaticName,
    vendorStaticName,
} from './config/path';

const inlineStyle = readFileSync(join(staticPath, stylesInlineName), 'utf8');

export function getHeaderString(): string {
    const helmet = Helmet.renderStatic();
    return '<!DOCTYPE html>' +
        `<html ${helmet.htmlAttributes.toString()}>` +
        '<head>' +
        helmet.meta.toString() +
        helmet.title.toString() +
        '<style type="text/css">' +
        inlineStyle +
        '</style>' +
        `<link rel="preload" href="${stylesStaticName}" as="style" onload="this.rel='stylesheet'">` +
        '</head>' +
        '<body>' +
        '<div id="root">';
}

export function getFooterString(): string {
    return '</div>' +
        `<script src="${vendorStaticName}" type="text/javascript" async></script>` +
        `<script src="${runtimeStaticName}" type="text/javascript" async></script>` +
        `<script src="${applicationStaticName}" type="text/javascript" defer></script>` +
        '</body>' +
        '<html>';
}
