import {ParameterizedContext} from 'koa';
import Router from 'koa-router';

import {SearchService} from '@service/Search';

const prefixSearch = '/search';

export function searchRoute(router: Router): void {
    router
        .get(prefixSearch, async (ctx: ParameterizedContext) => {
            return ctx.body = await SearchService.find(ctx);
        });
}
