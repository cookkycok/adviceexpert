import {ParameterizedContext} from 'koa';
import Router from 'koa-router';

import {UserService} from '@service/User';

const prefixUser = '/user';

export function userRoute(router: Router): void {
    router
        .post(`${prefixUser}/auth`, async (ctx: ParameterizedContext) => {
            return ctx.body = await UserService.authorization(ctx);
        })
        .put(`${prefixUser}/register`, async (ctx: ParameterizedContext) => {
            return ctx.body = await UserService.register(ctx);
        });
}
