import Router from '@koa/router';
import {checkRoute} from './check';
import {searchRoute} from './search';
import {userRoute} from './user';

const apiPath = '/api';

export const AppRoute = new Router({
    prefix: apiPath,
});

checkRoute(AppRoute);
searchRoute(AppRoute);
userRoute(AppRoute);
