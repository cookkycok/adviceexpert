import {ParameterizedContext} from 'koa';
import Router from 'koa-router';

export function checkRoute(router: Router): void {
    router
        .get('/check', async (ctx: ParameterizedContext) => {
            ctx.body = {
                site: true,
            };
        });
}
