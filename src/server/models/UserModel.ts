import mongoose, {Document, Schema} from "mongoose";

export const UserSchema = new Schema({
    email: {
        index: true,
        lowercase: true,
        required: true,
        type: String,
        unique: true,
    },
    name: {
        required: true,
        type: String,
    },
    password: {
        required: true,
        type: String,
    },
});

export const UserModel = mongoose.model<UserDocument>("User", UserSchema);
UserModel.createIndexes();

export interface UserDocument extends Document, UserRegistration {}

export interface UserRegistration {
    email: string;
    name: string;
    password: string;
}
