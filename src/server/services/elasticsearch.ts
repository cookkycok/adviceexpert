import Elasticsearch from 'elasticsearch';

import {elasticsearchConfig} from '@config/connect';

export const ElasticsearchService = new Elasticsearch.Client(elasticsearchConfig);

ElasticsearchService.ping({
    requestTimeout: 30000,
}, function(error) {
    if (error) {
        console.error('elasticsearch cluster is down!');
    } else {
        console.log('Elasticsearch is ok');
    }
});
