import mongoose from "mongoose";

import {mongodbConfig} from '@config/connect';
import './elasticsearch';

mongoose.connect(mongodbConfig, {useNewUrlParser: true});
