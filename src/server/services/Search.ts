import {ParameterizedContext} from 'koa';

import {getQueryData} from '@util/request';
import {answerOk, ServiceAnswer, unauthorizedError} from '@middleware/answer';
import {ElasticsearchService} from './elasticsearch';

class Search {
    public async find(ctx: ParameterizedContext): Promise<ServiceAnswer> {
        const {query} = getQueryData(ctx);
        try {
            const {hits: {total: {value: count}, hits: result}} = await ElasticsearchService.search({
                q: query,
            });
            return answerOk(ctx, {
                count,
                result,
            });
        } catch (error) {
            return unauthorizedError(ctx, error.message);
        }
    }
}

export const SearchService = new Search();
