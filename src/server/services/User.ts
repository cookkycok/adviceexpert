import {ParameterizedContext} from 'koa';
import bcrypt from 'bcrypt';

import {getBodyData} from '@util/request';
import {getDataRedis, setDataRedis} from '@service/redis';
import {isEmailValid, isPasswordValid, isNameValid} from '@util/validation';
import {MailServices, UserCountCheck, UserServiceErrorAnswer} from '../enums';
import {unauthorizedError, answerOk, ServerError, ServiceAnswer} from '@middleware/answer';
import {UserDocument, UserModel, UserRegistration} from '@model/UserModel';

class User {
	private saltRounds = 8;
	private timeExpireBlock = 15 * 60; // 15 minutes

	public async authorization(ctx: ParameterizedContext): Promise<ServiceAnswer> {
	    try {
	        const {email: emailBody = '', password: passwordAuth = ''} = <UserRegistration>getBodyData(ctx);
	        if (!isEmailValid(emailBody)) {
	            throw new ServerError(UserServiceErrorAnswer.invalidEmail, {email: UserServiceErrorAnswer.invalidEmail});
	        }
	        if (!isPasswordValid(passwordAuth)) {
	            throw new ServerError(UserServiceErrorAnswer.incorrect, {email: UserServiceErrorAnswer.incorrect});
	        }

	        const {state: {requestId}} = ctx;
	        const checkUserExistKey = UserCountCheck.checkUserExist + requestId;
	        const checkUserPasswordKey = UserCountCheck.checkUserPassword + requestId;
	        const countCheckUser = await getDataRedis<number>(checkUserExistKey) || 0;
	        const countCheckPassword = await getDataRedis<number>(checkUserPasswordKey) || 0;

	        if(countCheckUser > UserCountCheck.checkUserExistCount || countCheckPassword > UserCountCheck.checkUserPasswordCount) {
	            throw new ServerError(UserServiceErrorAnswer.blocked);
	        }

	        const email = User.clearEmailDots(emailBody);
	        const {_id, password} = (await UserModel.findOne({email}).exec() || {}) as UserDocument;

	        if (!_id) {
	        	await setDataRedis(checkUserExistKey, countCheckUser + 1, this.timeExpireBlock);
	            throw new ServerError(UserServiceErrorAnswer.userNotFound, {email: UserServiceErrorAnswer.userNotFound});
	        }

	        if(!User.comparePassword(passwordAuth, password)) {
	            await setDataRedis(checkUserPasswordKey, countCheckPassword + 1, this.timeExpireBlock);
	            throw new ServerError(UserServiceErrorAnswer.incorrect, {email: UserServiceErrorAnswer.incorrect});
	        }

	        return answerOk(ctx);
	    } catch (e) {
	        return unauthorizedError(ctx, e.message, e.attributes);
	    }
	}

	public async register(ctx: ParameterizedContext): Promise<ServiceAnswer> {
	    try {
	        const {email: emailBody = '', password = '', name = ''} = <UserRegistration>getBodyData(ctx);

	        if (!isEmailValid(emailBody)) {
	            throw new ServerError(UserServiceErrorAnswer.invalidEmail, {email: UserServiceErrorAnswer.invalidEmail});
	        }
	        if (!isPasswordValid(password)) {
	            throw new ServerError(UserServiceErrorAnswer.invalidPassword, {password: UserServiceErrorAnswer.invalidPassword});
	        }
	        if (!isNameValid(name)) {
	            throw new ServerError(UserServiceErrorAnswer.invalidName, {name: UserServiceErrorAnswer.invalidName});
	        }

	        const email = User.clearEmailDots(emailBody);
	        const isUserExist = Boolean(await UserModel.findOne({email}).exec());
	        if (isUserExist) {
	            throw new ServerError(UserServiceErrorAnswer.userExist, {email: UserServiceErrorAnswer.userExist});
	        }

	        const newUser = new UserModel({
	            email,
	            name,
	            password: this.encodePassword(password),
	        });
	        await newUser.save();

	        return answerOk(ctx);
	    } catch (e) {
	        return unauthorizedError(ctx, e.message, e.attributes);
	    }
	}

	private encodePassword(password: string): string {
	    return bcrypt.hashSync(password, this.saltRounds);
	}

	private static comparePassword(password: string, hash: string): boolean {
	    return bcrypt.compareSync(password, hash);
	}

	private static clearEmailDots(email: string): string {
	    const [name, domain] = email.split('@');
	    if (domain === MailServices.gmail) {
	        return [name.replace(/\./g, ''), domain].join('@');
	    }
	    return email;
	}
}

export const UserService = new User();
