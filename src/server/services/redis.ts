import Redis from 'ioredis';

import {redisConfig} from '@config/connect';

const redisService = new Redis(redisConfig);
const oneDayExpire = 24 * 60 * 60;

export const setDataRedis: AddData = (key, data, expireTime = oneDayExpire) => redisService.set(key, JSON.stringify(data), 'EX', expireTime);
export const getDataRedis: GetData = async (key) => {
    const data = await redisService.get(key);
    return data && JSON.parse(data);
};
export const delDataRedis: DeleteData = async (...key) => redisService.del(...key);

type TypeData = string | number | object;
interface AddData {
	<T = TypeData>(key: string, data: T, expireTime?: number): Promise<string>;
}
interface GetData {
	<T = TypeData>(key: string): Promise<T>;
}
interface DeleteData {
	(...key: string[]): Promise<number>;
}
