import Koa from 'koa';
import {createServer} from 'http';
import koaBody from 'koa-body';

import {AppRoute} from './routes';
import '@service/db';
import {initApplication} from '@middleware/initApplication';
import renderPage from '@middleware/renderPage';
import routeStart from '@middleware/routeStart';
import timeRequest from '@middleware/timeRequest';
import {initUser} from '@middleware/user';
import {socketService} from './socket';

const app = new Koa();

if (process.env.NODE_ENV !== 'production') {
    app.use(timeRequest);
}

app
    .use(initApplication)
    .use(initUser)
    .use(koaBody())
    .use(AppRoute.routes())
    .use(AppRoute.allowedMethods())
    .use(routeStart)
    .use(renderPage);

const server = createServer(app.callback());
socketService(server.listen(4000));
