import * as React from 'react';
import {ParameterizedContext} from 'koa';
import {renderToString} from 'react-dom/server';

import {getHeaderString, getFooterString} from '../html';
import {Application} from '../../client/helpers/root';

export default async function renderPage(ctx: ParameterizedContext): Promise<void> {
    const renderPage = renderToString(<Application router={ctx.state.router} />);
    ctx.status = 200;
    ctx.body = getHeaderString() + renderPage + getFooterString();
}
