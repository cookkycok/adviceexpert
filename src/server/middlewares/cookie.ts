import {ParameterizedContext} from 'koa';
import Cookies from 'cookies';

import {TNext} from '@middleware/interface';

function getFutureDate(): Date {
    const d = new Date();
    d.setDate(d.getDate() + 70);
    return d;
}

export function getSetCookieConfig(ctx: ParameterizedContext, options?: Cookies.SetOption): Cookies.SetOption {
    const [domain] = ctx.host.split(':');
    return {
        domain,
        expires: getFutureDate(),
        httpOnly: false,
        ...options,
    };
}

function getDefaultRemoveConfig(ctx: ParameterizedContext): Cookies.SetOption {
    const [domain] = ctx.host.split(':');
    return {
        domain,
        expires: new Date(1),
        maxAge: 1,
    };
}

export function removeCookie(ctx: ParameterizedContext, next: TNext) {
    return async (key: string): Promise<void> => {
        ctx.cookies.set(key, '', getDefaultRemoveConfig(ctx));
        await next();
    };
}

