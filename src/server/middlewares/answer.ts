import {ParameterizedContext} from 'koa';

export function answerOk<T = object>(ctx: ParameterizedContext, data?: T): SuccessAnswer<T> {
    ctx.status = 200;
    return {
        data,
        status: ctx.status,
    };
}

export function unauthorizedError<T = object>(ctx: ParameterizedContext, message: string, attributes?: Attributes<T>): ErrorAnswer<T> {
    ctx.status = 401;
    return {
        attributes,
        body: ctx.request.body,
        error: "Unauthorized",
        message,
        status: ctx.status,
    };
}

export class ServerError extends Error{
    public attributes: object;

    constructor(message: string, attributes: object = {}) {
        super();
        this.attributes = attributes;
        this.message = message;
    }
}

export type ServiceAnswer<T = object> = SuccessAnswer<T> | ErrorAnswer<T>;
export type Attributes<T> = {
    [key in keyof T]: string
}

interface ErrorAnswer<T> {
    attributes?: Attributes<T>;
    body: T;
    error: string;
    message: string;
    status: number;
}

interface SuccessAnswer<T> {
    data?: T;
    status: number;
}
