import {cloneRouter} from 'router5';

import createRoute from '../../client/route';
import {ParameterizedContext} from 'koa';
import {TNext} from './interface';

const mainRoute = createRoute();

export default function routeStart(ctx: ParameterizedContext, next: TNext): void {
    const router = cloneRouter(mainRoute);
    router.start(ctx.request.url, error => {
        if (error) {
            ctx.status = 404;
            return ctx.body = 'Not found!';
        }
        ctx.state.router = router;
        return next();
    });
}
