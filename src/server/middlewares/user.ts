import {ParameterizedContext} from 'koa';

import {TNext} from './interface';
import {CookieKeys} from '../enums';
import {decodeUserId} from '@util/user';
import {getSetCookieConfig} from '@middleware/cookie';

export async function initUser(ctx: ParameterizedContext, next: TNext): Promise<void> {
    ctx.state.user = {};

    const cookieToken = ctx.cookies.get(CookieKeys.token);
    if(cookieToken) {
        const {data: userId} = decodeUserId(cookieToken);
        ctx.state.user.userId = userId;
        ctx.cookies.set(CookieKeys.token, cookieToken, getSetCookieConfig(ctx));
    }
    await next();
}
