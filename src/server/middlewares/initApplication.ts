import {ParameterizedContext} from 'koa';
import uuid from 'uuid/v4';

import {TNext} from './interface';
import {getSetCookieConfig, removeCookie} from '@middleware/cookie';
import {CookieKeys} from '../enums';

const header = 'X-Request-Id';

export async function initApplication(ctx: ParameterizedContext, next: TNext): Promise<void> {
    const reqId = ctx.get(CookieKeys.request) || ctx.cookies.get(CookieKeys.request) || uuid();

    ctx.id = reqId;
    ctx.state.requestId = reqId;
    ctx.set(header, reqId);
    ctx.cookies.set(CookieKeys.request, reqId, getSetCookieConfig(ctx));

    // eslint-disable-next-line @typescript-eslint/ban-ts-ignore
    // @ts-ignore
    ctx.cookies.del = removeCookie(ctx, next);
    await next();
}
