import {ParameterizedContext} from 'koa';

export default async function timeRequest(ctx: ParameterizedContext, next): Promise<void> {
    const start = Date.now();
    await next();
    const ms = Date.now() - start;
    console.log(`${ctx.method} ${ctx.url} - ${ms}ms`);
}
