import {join} from 'path';

const STATIC_DIR = '/static/';
const IS_PROD = process.env.NODE_ENV === 'production';

export const staticPath = join(__dirname, '..', STATIC_DIR);

const {
    assetsByChunkName: {application, runtime, vendors},
// eslint-disable-next-line @typescript-eslint/no-var-requires
} = require(join(staticPath, 'stats.json'));

export const applicationStaticName = STATIC_DIR + (IS_PROD ? application[1] : 'application.js');
export const runtimeStaticName = STATIC_DIR + (IS_PROD ? runtime : 'runtime.js');
export const stylesStaticName = STATIC_DIR + (IS_PROD ? application[0] : 'application.css');
export const stylesInlineName = application[0];
export const vendorStaticName = STATIC_DIR + (IS_PROD ? vendors : 'vendors.js');
