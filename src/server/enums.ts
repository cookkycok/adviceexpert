export enum CookieKeys {
	request = 'X-Request-Id',
	token = 'token',
}

export enum UserServiceErrorAnswer {
	blocked = 'Request blocked. Try later.',
	incorrect = 'Email or password incorrect',
	invalidEmail = 'Invalid email',
	invalidName = 'Invalid name',
	invalidPassword = 'Invalid password',
	userExist = 'User already exist',
	userNotFound = 'User not found',
}

export enum UserCountCheck {
	checkUserExist = 'checkUserExist',
	checkUserExistCount = 5,
	checkUserPassword = 'checkUserPassword',
	checkUserPasswordCount = 3,
}

export enum MailServices {
	gmail = 'gmail.com'
}
