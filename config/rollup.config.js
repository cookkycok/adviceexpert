import path from 'path';
import {readFileSync} from 'fs';
import babel from 'rollup-plugin-babel';
import filesize from 'rollup-plugin-filesize';
import nodeResolve from 'rollup-plugin-node-resolve';
import progress from 'rollup-plugin-progress';
import typescript from 'rollup-plugin-typescript';
import replace from 'rollup-plugin-replace';
import postcss from 'rollup-plugin-postcss';
import json from 'rollup-plugin-json';
import commonjs from 'rollup-plugin-commonjs';
import clear from 'rollup-plugin-clear';
import alias from 'rollup-plugin-alias';
import reactSvg from "rollup-plugin-react-svg";

import postcssNamePlugin from './postcssNamePlugin';
import {dependencies} from '../package.json';

const external = Object.keys(dependencies).concat(['fs', 'path', 'http', 'react-dom/server', 'react-hot-loader/root', 'uuid/v4']);
const env = process.env.NODE_ENV || 'development';
const isProd = process.env.NODE_ENV === 'production';
const sourceSrc = path.join(__dirname, '../src');
const sourcePath = path.join(sourceSrc, 'client');
const sourceServerPath = path.join(sourceSrc, 'server');

module.exports = {
    input: 'src/server/main.tsx',
    output: [
        {
            dir: 'dist/server',
            format: 'cjs',
        },
    ],
    plugins: [
        progress(),
        clear({
            targets: ['dist/server'],
            watch: true,
        }),
        nodeResolve({
            browser: false,
            main: true,
            modulesOnly: true,
            preferBuiltins: true,
        }),
        replace({
            'process.env.LOCALE': JSON.stringify({
                en: JSON.parse(readFileSync(path.join(sourceSrc, 'locale', 'en.json')).toString()),
                ru: JSON.parse(readFileSync(path.join(sourceSrc, 'locale', 'ru.json')).toString()),
                uk: JSON.parse(readFileSync(path.join(sourceSrc, 'locale', 'uk.json')).toString()),
            }),
            'process.env.SERVER': JSON.stringify(true),
        }),
        alias({
            resolve: ['.ts', '.tsx', '.json', '.svg'],
            entries: [
                {
                    find: '@pages',
                    replacement: path.join(sourcePath, 'pages/index'),
                },
                {
                    find: /^@locale$/,
                    replacement: path.join(sourceSrc, 'locale/index'),
                },
                {
                    find: '@store',
                    replacement: path.join(sourcePath, 'stores/index'),
                },
                {
                    find: /^@page(.*)/,
                    replacement: path.join(sourcePath, 'pages$1/index'),
                },
                {
                    find: /^@route(.*)/,
                    replacement: path.join(sourcePath, 'route$1'),
                },
                {
                    find: /^@icon(.*)/,
                    replacement: path.join(sourceSrc, 'assets', 'icons$1'),
                },
                {
                    find: /^@locale(.*)/,
                    replacement: path.join(sourceSrc, 'locale$1'),
                },
                {
                    find: /^@config(.*)/,
                    replacement: path.join(sourceServerPath, 'config$1'),
                },
                {
                    find: /^@middleware(.*)/,
                    replacement: path.join(sourceServerPath, 'middlewares$1'),
                },
                {
                    find: /^@util(.*)/,
                    replacement: path.join(sourceServerPath, 'utils$1'),
                },
                {
                    find: /^@service(.*)/,
                    replacement: path.join(sourceServerPath, 'services$1'),
                },
            ],
        }),
        json({
            preferConst: false, // Default: false
            indent: ' ',
            compact: true, // Default: false
            namedExports: false, // Default: true
        }),
        typescript({
            tsconfig: path.join(__dirname, '..', 'tsconfig.back.json'),
        }),
        babel({
            babelrc: false,
            presets: [
                [
                    '@babel/preset-env',
                    {
                        targets: {
                            node: 'current',
                        },
                    },
                ],
                'optimizations',
            ],
            plugins: [
                '@babel/plugin-syntax-dynamic-import',
                '@babel/plugin-transform-react-constant-elements',
                '@babel/plugin-transform-react-inline-elements',
                'transform-react-remove-prop-types',
                'babel-plugin-loop-optimizer',
                'closure-elimination',
            ],
        }),
        postcss({
            use: ['sass'],
            syntax: 'postcss-scss',
            parser: 'postcss-scss',
            ident: 'postcss',
            autoModules: false,
            extract: false,
            inject: false,
            modules: true,
            namedExports: true,
            writeDefinitions: true,
            plugins: [
                require('postcss-import')(),
                require('postcss-preset-env')(),
                require('postcss-modules-values')(),
                require('postcss-node-modules-replacer')(),
                require('rucksack-css')(),
                require('autoprefixer'),
                postcssNamePlugin(isProd),
                require('cssnano')(),
            ],
        }),
        reactSvg({
            jsx: false,
            svgo: {
                floatPrecision: 2,
            },
        }),
        commonjs(),
        filesize(),
    ],
    external,
};
