import {Base64} from 'js-base64';

export default function(isProd) {
    return (css) => {
        css.walkRules(ruleNode => {
            if(ruleNode.selector === ':export') {
                const {prop} = ruleNode.nodes[0];
                ruleNode.nodes[0].value = isProd ? Base64.encode(prop + ruleNode.parent.source.input.file).substring(0, 6) : prop;
            }
            return ruleNode;
        });
        return css;
    };
}
