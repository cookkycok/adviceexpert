const {LokaliseApi} = require('@lokalise/node-api');
const http = require('https');
const fs = require('fs');
const {join} = require('path');
const unzipper = require('unzipper');

const lokaliseApi = new LokaliseApi({apiKey: 'd7d15dcabd23aed0ed1150e39753611911cc5380'});
const pathToZip = join(__dirname, '..', '.cache', 'locale.zip');
const pathToLocale = join(__dirname, '..', 'src');
const mainLang = 'en';

const download = (url, path) => new Promise((resolve, reject) => {
    http.get(url, response => {
        const statusCode = response.statusCode;

        if (statusCode !== 200) {
            // eslint-disable-next-line prefer-promise-reject-errors
            return reject('Download error!');
        }

        const writeStream = fs.createWriteStream(path);
        response.pipe(writeStream);

        // eslint-disable-next-line prefer-promise-reject-errors
        writeStream.on('error', () => reject('Error writing to file!'));
        writeStream.on('finish', () => writeStream.close(resolve));
    });}).catch(err => console.error(err));

(async function() {

    const [project] = await lokaliseApi.projects.list();
    const {project_id} = project;
    const {bundle_url} = await lokaliseApi.files.download(project_id, {format: 'json', "original_filenames": false});
    const keys = (await lokaliseApi.translations.list({project_id})).map(({language_iso}) => language_iso);

    await download(bundle_url, pathToZip);

    await fs.createReadStream(pathToZip)
        .pipe(unzipper.Extract({path: pathToLocale}))
        .on('entry', entry => entry.autodrain())
        .promise();

    fs.unlinkSync(pathToZip);

    const mainLocaleData = require(join(pathToLocale, 'locale', `en.json`));
    const mainLocaleDataKeys = Object.keys(mainLocaleData);
    fs.writeFileSync(join(pathToLocale, 'locale', `en.json`), JSON.stringify(mainLocaleData));

    keys
        .filter((key) => key !== mainLang)
        .forEach((key) => {
            const pathFile = join(pathToLocale, 'locale', `${key}.json`);
            const localeData = require(pathFile);
            mainLocaleDataKeys.forEach((localeKey) => {
                localeData[localeKey] = localeData[localeKey] || mainLocaleData[localeKey];
            });
            fs.writeFileSync(pathFile, JSON.stringify(localeData));
        });
})();
