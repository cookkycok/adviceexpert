const path = require('path');
const webpack = require('webpack');
const WebpackCleanupPlugin = require('webpack-cleanup-plugin');
const TerserPlugin = require('terser-webpack-plugin');
const WebpackMonitor = require('webpack-monitor');
const Base64 = require('js-base64').Base64;
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const OptimizeCSSAssetsPlugin = require('optimize-css-assets-webpack-plugin');
const {StatsWriterPlugin} = require("webpack-stats-plugin");

const isProduction =
    process.argv.indexOf('-p') >= 0 || process.env.NODE_ENV === 'production';
const mainDir = path.join(__dirname, '..');
const mainSourcePath = path.join(mainDir, 'src');
const sourcePath = path.join(mainSourcePath, 'client');
const sourceDist = path.join(mainDir, 'dist');

const babelLoader = {
    loader: 'babel-loader',
    options: {
        presets: [
            [
                '@babel/preset-env',
                {
                    useBuiltIns: 'entry',
                    targets: '> 1%, last 3 version',
                    corejs: '3.2.1',
                    shippedProposals: true,
                },
            ],
            'optimizations',
        ],
        plugins: [
            'react-hot-loader/babel',
            '@babel/plugin-syntax-dynamic-import',
            '@babel/plugin-transform-react-constant-elements',
            '@babel/plugin-transform-react-inline-elements',
            'transform-react-remove-prop-types',
            'babel-plugin-loop-optimizer',
            'closure-elimination',
        ],
    },
};

const cacheLoader = {
    loader: 'cache-loader',
    options: {
        cacheDirectory: path.join(mainDir, '.cache'),
    },
};

const config = {
    entry: {
        application: [
            path.join(sourcePath, 'styles', 'root.scss'),
            path.join(sourcePath, 'index.tsx'),
        ],
    },
    output: {
        path: path.join(sourceDist, 'static'),
        filename: isProduction ? '[name].[contenthash].js' : '[name].js',
        chunkFilename: isProduction ? '[name].[contenthash].js' : '[name].js',
        publicPath: '/static/',
    },
    target: 'web',
    resolve: {
        extensions: ['.js', '.ts', '.tsx', '.scss', '.css'],
        mainFields: ['jsnext:main', 'module', 'browser', 'main'],
        alias: {
            '@icon': path.resolve(mainDir, 'src/assets/icons'),
            '@locale': path.resolve(mainDir, 'src', 'locale'),
            '@middleware': path.resolve(sourcePath, '../server', 'middlewares'),
            '@page': path.resolve(sourcePath, 'pages'),
            '@page-names': path.resolve(sourcePath, 'route', 'routes'),
            '@pages': path.resolve(sourcePath, 'pages', 'index'),
            '@route': path.resolve(sourcePath, 'route'),
            '@store': path.resolve(sourcePath, 'stores', 'index'),
            '@util': path.resolve(sourcePath, 'utils'),
        },
    },
    module: {
        rules: [
            {
                test: /\.tsx?$/,
                include: [
                    mainSourcePath,
                ],
                exclude: /(node_modules|server)/,
                use: [
                    cacheLoader,
                    babelLoader,
                    {
                        loader: 'ts-loader',
                        options: {
                            configFile: path.resolve(mainDir, 'tsconfig.json'),
                        },
                    },
                ],
            },
            {
                test: /\.s[ac]ss$/i,
                include: [
                    mainSourcePath,
                ],
                exclude: /(node_modules|server)/,
                use: [
                    {
                        loader: MiniCssExtractPlugin.loader,
                        options: {
                            hmr: !isProduction,
                        },
                    },
                    cacheLoader,
                    {
                        loader: 'css-loader',
                        options: {
                            // modules: true,
                            importLoaders: 1,
                            url: false,
                            sourceMap: true,
                            modules: {
                                getLocalIdent: (context, localIdentName, localName) => isProduction ? Base64.encode(localName + context.resourcePath).substring(0, 6) : localName,
                            },
                        },
                    },
                    {
                        loader: 'postcss-loader',
                        options: {
                            use: ['sass'],
                            syntax: 'postcss-scss',
                            parser: 'postcss-scss',
                            ident: 'postcss',
                            sourceMap: true,
                            modules: true,
                            namedExports: true,
                            writeDefinitions: true,
                            plugins: [
                                require('postcss-import')(),
                                require('postcss-preset-env')(),
                                require('postcss-modules-values')(),
                                require('postcss-node-modules-replacer')(),
                                require('rucksack-css')(),
                                require('autoprefixer'),
                                isProduction ? require('cssnano')() : require("postcss-reporter")({clearReportedMessages: true}),
                            ],
                        },
                    },
                ],
            },
            {
                test: /\.svg$/,
                include: [
                    mainSourcePath,
                ],
                exclude: /(node_modules|server)/,
                use: [
                    cacheLoader,
                    babelLoader,
                    {
                        loader: "react-svg-loader",
                        options: {
                            jsx: false,
                            svgo: {
                                floatPrecision: 2,
                            },
                        },
                    },
                ],
            },
        ],
    },
    plugins: [
        new webpack.EnvironmentPlugin({
            NODE_ENV: isProduction ? 'production' : 'development', // use 'development' unless process.env.NODE_ENV is defined
            DEBUG: false,
        }),
        new WebpackCleanupPlugin(),
        new MiniCssExtractPlugin({
            filename: isProduction ? '[name].[contenthash].css' : '[name].css',
            ignoreOrder: false,
        }),
    ],
    optimization: {
        mangleWasmImports: true,
        sideEffects: true,
        splitChunks: {
            chunks: 'async',
            minSize: 30000,
            maxSize: 0,
            minChunks: 1,
            maxAsyncRequests: 5,
            maxInitialRequests: 3,
            name: true,
            cacheGroups: {
                vendors: {
                    test: /[\\/]node_modules[\\/]/,
                    chunks: 'all',
                    priority: -10,
                    name: 'vendors',
                    filename: isProduction ? '[name].[contenthash].js' : '[name].js',
                },
            },
        },
        runtimeChunk: {
            name: 'runtime',
        },
    },
    resolveLoader: {
        modules: [
            'node_modules',
        ],
    },
    devServer: {
        headers: {
            'Access-Control-Allow-Origin': '*',
        },
        disableHostCheck: true,
        hotOnly: true,
        proxy: {
            '**': 'http://localhost:4000',
        },
    },
    devtool: !isProduction && 'cheap-module-eval-source-map',
    node: {
        fs: 'empty',
        net: 'empty',
    },
};

if (isProduction) {
    config.optimization.minimizer = [
        new TerserPlugin({
            parallel: true,
            cache: path.join(mainDir, '.cache'),
            sourceMap: !isProduction,
        }),
        new OptimizeCSSAssetsPlugin({}),
        new StatsWriterPlugin({
            filename: "stats.json",
        }),
    ];
    config.plugins.push(new WebpackMonitor({
        capture: true,
        target: path.join(mainDir, '.cache', 'clientStatsStore.json'),
        launch: process.env.STATS === 'true',
        port: 3030,
        excludeSourceMaps: true,
    }));
}

module.exports = config;
