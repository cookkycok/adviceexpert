const {readdirSync, readFileSync, writeFileSync} = require('fs');
const {join, basename} = require('path');

const pathLocale = join(__dirname, '../src', 'locale');

function generateInterface() {

    const files = readdirSync(pathLocale).filter((file) => file.indexOf('json') !== -1);
    const jsonData = JSON.parse(readFileSync(join(pathLocale, files[0])).toString());
    const localeKeys = Object.keys(jsonData);
    const locales = files.map((file) => basename(file, '.json'));

    let exportInterfaceFile = 'export enum LocaleKeys {\n';
    localeKeys.forEach((key) => {
        exportInterfaceFile += `    ${key} = '${key}',\n`;
    });
    exportInterfaceFile += '}\n\n';

    exportInterfaceFile += 'export enum LocaleLangs {\n';
    locales.forEach((key) => {
        exportInterfaceFile += `    ${key} = '${key}',\n`;
    });
    exportInterfaceFile += '}\n\n';

    exportInterfaceFile += 'export type Locale = {\n';
    exportInterfaceFile += '    [key in keyof LocaleKeys]: string;\n';
    exportInterfaceFile += '}\n\n';

    exportInterfaceFile += 'export type LocaleGlobal = {\n';
    exportInterfaceFile += '    [lang in keyof LocaleLangs]: Locale;\n';
    exportInterfaceFile += '}\n';

    writeFileSync(join(pathLocale, 'interface.ts'), exportInterfaceFile);
}

generateInterface();
