{
    "parser": "@typescript-eslint/parser",
    "parserOptions": {
        "ecmaVersion": 2018,
        "sourceType": "module",
        "ecmaFeatures": {
            "jsx": true,
            "modules": true
        },
        "useJSXTextNode": true
    },
    "env": {
        "browser": true,
        "es6": true,
        "node": true,
        "commonjs": true
    },
    "extends": [
        "eslint:recommended",
        "koa",
        "plugin:@typescript-eslint/recommended",
        "plugin:react/recommended",
        "prettier"
    ],
    "plugins": [
        "@typescript-eslint",
        "react",
        "simple-import-sort",
        "typescript-sort-keys"
    ],
    "rules": {
        "array-bracket-spacing": ["error", "never"],
        "comma-dangle": ["error", "always-multiline"],
        "comma-style": ["error", "last"],
        "computed-property-spacing": ["error", "never"],
        "indent": ["error", 4],
        "object-curly-spacing": ["error", "never"],
        "semi": ["error", "always"],
        "no-new-wrappers": "error",
        "sort-keys": "error",
        "sort-vars": "error",
        "require-atomic-updates": 0,

        // React
        "react/jsx-sort-props": "error",
        "react/boolean-prop-naming": "error",
        "react/destructuring-assignment": ["error", "always"],
        "react/no-access-state-in-setstate": "error",
        "react/no-array-index-key": "warn",
        "react/no-children-prop": "error",
        "react/no-danger": "warn",
        "react/no-danger-with-children": "warn",
        "react/no-deprecated": "error",
        "react/no-direct-mutation-state": "error",
        "react/no-is-mounted": "error",
        "react/no-redundant-should-component-update": "error",
        "react/no-string-refs": "error",
        "react/no-this-in-sfc": "error",
        "react/no-unescaped-entities": "error",
        "react/no-unknown-property": "warn",
        "react/no-unsafe": "error",
        "react/no-unused-state": "error",
        "react/no-will-update-set-state": "error",
        "react/prefer-es6-class": ["error", "always"],
        "react/react-in-jsx-scope": "error",
        "react/require-render-return": "error",
        "react/self-closing-comp": "error",
        "react/sort-comp": ["error", {
            "order": [
                "static-methods",
                "lifecycle",
                "render",
                "everything-else"
            ]
        }],
        "react/state-in-constructor": ["error", "never"],
        "react/static-property-placement": "error",
        "react/style-prop-object": "error",

        // JSX
        "react/jsx-boolean-value": "error",
        "react/jsx-child-element-spacing": "error",
        "react/jsx-closing-bracket-location": "error",
        "react/jsx-closing-tag-location": "error",
        "react/jsx-curly-spacing": ["error", "never"],
        "react/jsx-equals-spacing": ["error", "never"],
        "react/jsx-filename-extension": [1, { "extensions": [".js", ".jsx", ".ts", ".tsx"] }],
        "react/jsx-first-prop-new-line": ["error", "multiline"],
        "react/jsx-handler-names": "error",
        "react/jsx-max-depth": ["warn", { "max": 5 }],
        "react/jsx-max-props-per-line": [2, { "maximum": 1 }],
        "react/jsx-no-bind": "error",
        "react/jsx-no-comment-textnodes": "error",
        "react/jsx-no-undef": "error",
        "react/jsx-one-expression-per-line": "error",
        "react/jsx-fragments": ["error", "syntax"],
        "react/jsx-pascal-case": "error",
        "react/jsx-props-no-multi-spaces": "error",
        "react/jsx-sort-default-props": "error",
        "react/jsx-wrap-multilines": "error",

        // Typescript
        "typescript-sort-keys/interface": 2,
        "typescript-sort-keys/string-enum": 2,
        "@typescript-eslint/consistent-type-assertions": 0
    }
}
